-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 17 2020 г., 03:39
-- Версия сервера: 5.6.37
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `laravel`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `title`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'кат 1', 'kat-1', '2019-10-23 20:47:56', '2019-10-23 20:47:56'),
(2, 'кат 2', 'kat-2', '2019-10-23 20:48:02', '2019-10-23 20:48:02'),
(3, 'кат 3', 'kat-3', '2019-10-23 20:48:06', '2019-10-23 20:48:06'),
(4, 'sint', 'sint', '2019-10-29 19:24:35', '2019-10-29 19:24:35'),
(5, 'est', 'est', '2019-10-29 19:25:34', '2019-10-29 19:25:34'),
(6, 'culpa', 'culpa', '2019-10-29 19:25:34', '2019-10-29 19:25:34'),
(7, 'facilis', 'facilis', '2019-10-29 19:25:34', '2019-10-29 19:25:34'),
(8, 'quia', 'quia', '2019-10-29 19:25:34', '2019-10-29 19:25:34');

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id`, `text`, `user_id`, `post_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'fdfdfdff', 2, 9, 1, '2020-01-09 18:12:42', '2020-01-09 19:57:57');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(27, '2014_10_12_000000_create_users_table', 1),
(28, '2014_10_12_100000_create_password_resets_table', 1),
(29, '2019_09_24_080019_create_categories_table', 1),
(30, '2019_09_24_081913_create_tags_table', 1),
(31, '2019_09_24_082203_create_comments_table', 1),
(32, '2019_09_24_082219_create_posts_table', 1),
(33, '2019_09_24_082254_create_subscriptions_table', 1),
(34, '2019_09_25_062100_create_posts_tags_table', 1),
(35, '2019_10_18_012605_add_avatar_column_to_users', 1),
(36, '2019_10_22_020656_make_password_nullable', 1),
(37, '2019_10_25_053643_add_date_to_posts', 2),
(38, '2019_10_25_062310_add_image_to_posts', 3),
(39, '2019_10_30_071054_add_description_to_posts', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `is_featured` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `date` date DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `content`, `category_id`, `user_id`, `status`, `views`, `is_featured`, `created_at`, `updated_at`, `date`, `image`, `description`) VALUES
(9, 'Пост 1', 'post-1', '<p>vgfdsfsd fds</p>', NULL, 2, 1, 0, 1, '2019-10-28 15:13:29', '2019-10-29 22:00:20', '2019-10-31', 'rY7bpjqc7n.jpeg', '<p><strong>rerfsdfsdfd</strong></p>'),
(12, 'fdsfsd', 'fdsfsd', '<p>dfgfd gfdg gfd</p>', 2, 3, 1, 0, 1, '2019-10-28 15:35:16', '2019-12-15 19:21:00', '2019-11-02', 'Fhakmg2n4G.jpeg', NULL),
(13, 'Iste quia pariatur iste.', 'iste-quia-pariatur-iste', '<p>Possimus et tenetur dolores dolorem.</p>', 1, 4, 0, 1434, 0, '2019-10-29 19:30:09', '2019-11-26 20:43:50', '2017-09-08', 'photo1.png', NULL),
(14, 'Consequatur cupiditate qui consectetur dolorem debitis quisquam.', 'consequatur-cupiditate-qui-consectetur-dolorem-debitis-quisquam', 'Consequatur nobis impedit explicabo distinctio et.', 1, 2, 1, 2778, 0, '2019-10-29 19:30:09', '2019-10-29 19:30:09', '2017-09-08', 'photo1.png', NULL),
(15, 'Maxime et eos accusamus unde doloribus aspernatur.', 'maxime-et-eos-accusamus-unde-doloribus-aspernatur', 'Adipisci ratione fugit sed consectetur in pariatur fugiat deserunt.', 1, 3, 1, 776, 0, '2019-10-29 19:30:09', '2019-10-29 19:30:09', '2017-09-08', 'photo1.png', NULL),
(16, 'Suscipit harum corrupti consequuntur ullam.', 'suscipit-harum-corrupti-consequuntur-ullam', 'Id officiis voluptas ipsum omnis ipsum totam.', 1, 4, 1, 2205, 0, '2019-10-29 19:30:09', '2019-10-29 19:30:09', '2017-09-08', 'photo1.png', NULL),
(17, 'Minus laborum repellat est est nobis velit dolores.', 'minus-laborum-repellat-est-est-nobis-velit-dolores', 'Dolores eligendi qui accusamus beatae necessitatibus modi quam.', 1, 2, 1, 1571, 0, '2019-10-29 19:30:09', '2019-10-29 19:30:09', '2017-09-08', 'photo1.png', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `post_tags`
--

CREATE TABLE `post_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `post_tags`
--

INSERT INTO `post_tags` (`id`, `post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(5, 7, 1, NULL, NULL),
(7, 7, 3, NULL, NULL),
(8, 8, 2, NULL, NULL),
(9, 8, 1, NULL, NULL),
(10, 8, 3, NULL, NULL),
(11, 9, 1, NULL, NULL),
(12, 9, 2, NULL, NULL),
(13, 10, 2, NULL, NULL),
(14, 10, 3, NULL, NULL),
(15, 11, 1, NULL, NULL),
(16, 11, 2, NULL, NULL),
(17, 11, 3, NULL, NULL),
(18, 12, 1, NULL, NULL),
(19, 12, 2, NULL, NULL),
(20, 13, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `email`, `token`, `created_at`, `updated_at`) VALUES
(1, 'aaasddd@mail.ru', '1VK1LixEvQLBDUcdHOQO7UzQ4RxOnWHx3BhaWoOiyrNgNCldZj4evgDtqRibV62MzYslAgUn5tmuEf2uLAm3TRG0FWwAjTrdskhU', '2020-01-09 21:02:22', '2020-01-09 21:02:22'),
(3, 'web@lmg.group', 'sNmQFy5SflQclR5PPanUyvNVjiMgb7OvfLO5XdPnjPnaQbzChUXkRxmvK8m9UcfRh3Fqij4nfmh3JW8bZvojEuwX8rLyIApmYq1z', '2020-01-09 21:19:56', '2020-01-09 21:19:56'),
(4, 'vcxsfdf@mail.ru', 'Rq27CiWY2SqRZevN8DrWXJn9PIhplun3auPEy0qCTbaMcQ2Q5VrnQ0cT3kJJplTSfJVYxa93GPL87thPA7kr2WPJMJLfPj4UW0Qc', '2020-01-09 21:26:14', '2020-01-09 21:26:14'),
(5, 'gfgvffgfd@mail.ru', 'S7kswnpqTILY2klScPdI6pmoSHrXhTrKzeMfdbNVXGHC67ITlGvGQ6Iz3MId1CU60tLrqDFX3TpwXjQZMVYQHcX9x5SHpQ5L21iH', '2020-01-09 21:27:09', '2020-01-09 21:27:09'),
(7, 'dsadsa@mail.ru', 'wMenaXSozGA8SAbH1PQP5XaI696PPaHGV3Ps1c6opd2I9ICyrKuoye7WhBPOtGrwOMBtZSCbrRICqbKPvi8ridsIjYU3rLHv4raR', '2020-01-09 21:32:32', '2020-01-09 21:32:32'),
(8, 'vcxsdfgfds@mail.ru', 'PLjzmpLAF5l8WBkx7ZXcN66Xycf3MtPmyVADus9QBFiFmLA6y11RaTAxxmciM8CY1aIIiRYFiNhXaj7cpiGrI7FkjNH45Rt9o5VP', '2020-01-09 21:33:56', '2020-01-09 21:33:56'),
(9, 'dsaddascxzz@mail.ru', 'D1cTDgRZP5hzrYPNx0TDrqIV4mCZQiGwghfCeesQ3KinkcJbuncGAonLLxMXFj32rkG0bhNK8L9fnWUL9m4MqMJps0IsHtrYGEw0', '2020-01-09 21:35:28', '2020-01-09 21:35:28'),
(10, 'vcxvxcvxv@mail.ru', 's8MKtNWkwBaHPVFX5cJC1A8VUSFsBxUc3DY37VIcfM0iIs4N3U2mvcLVe5M2oj6FLOhP35r0qx7Xwm0cvlXoWns2nljsYNBKQbJW', '2020-01-09 21:38:15', '2020-01-09 21:38:15'),
(11, 'fsfdsfsd@mail.ru', NULL, '2020-01-09 22:21:59', '2020-01-09 22:27:53');

-- --------------------------------------------------------

--
-- Структура таблицы `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `tags`
--

INSERT INTO `tags` (`id`, `title`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'tag 1', 'tag-1', '2019-10-23 20:47:16', '2019-10-23 20:47:16'),
(2, 'Тег 2', 'teg-2', '2019-10-23 20:47:26', '2019-10-23 20:47:26'),
(3, 'тег 3', 'teg-3', '2019-10-23 20:47:34', '2019-10-23 20:47:34'),
(4, 'accusantium', 'accusantium', '2019-10-29 19:26:16', '2019-10-29 19:26:16'),
(5, 'sed', 'sed', '2019-10-29 19:26:16', '2019-10-29 19:26:16'),
(6, 'eos', 'eos', '2019-10-29 19:26:17', '2019-10-29 19:26:17'),
(7, 'magni', 'magni', '2019-10-29 19:26:17', '2019-10-29 19:26:17'),
(8, 'fuga', 'fuga', '2019-10-29 19:26:17', '2019-10-29 19:26:17');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `is_admin`, `status`, `remember_token`, `created_at`, `updated_at`, `avatar`) VALUES
(2, '1234', '123@mail.ru', '$2y$10$jiQy5vIaPO1H5VZoQIhVBuGeovOQhIhUzY6plRBte4nLx550JhmZ2', 1, 0, 'LudAA99WhKbldBk7ZDgWWoiGgYvDNMRGbcX0LOHhz4eyU6ig9cvw5gu49WXP', '2019-10-28 15:07:00', '2020-01-08 21:26:50', 'lG3O9lZiLp.png'),
(3, 'name13', 'aaasd@mail.ru', '$2y$10$pr4SCnzZRiSHgAwbhCE4a.cXvLs1qy6xZjWk8LDX6P/azyGWCqJwK', 0, 0, 'pc4wQsLutKScNKasP6ktREzTrdB1Ph5I9jCzxSxZnhhgO67bZTlXErnYVNl6', '2019-12-15 21:52:43', '2020-01-15 21:47:42', NULL),
(4, 'name2', 'aaasddd@mail.ru', '$2y$10$xk5nzwL82/u3GIxBIzns8Omw.4FNOJzS.5bJdAjit5cZK.3fnjU0u', 0, 0, NULL, '2019-12-15 21:54:51', '2019-12-15 21:54:51', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `post_tags`
--
ALTER TABLE `post_tags`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `post_tags`
--
ALTER TABLE `post_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
